import javax.print.attribute.standard.DateTimeAtCompleted;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.EventListener;

/**
 * Created by rchwhm on 2016/10/10.
 */
public class Result extends JFrame implements ActionListener {
    Result(){
    }
    JPanel resultPanel = new JPanel();
    JButton query1;
    JButton query2;
    JButton query3;

    JScrollBar sbh = new JScrollBar(JScrollBar.HORIZONTAL);
    ArrayList<JButton> buttons = new ArrayList<JButton>();
    ArrayList<JTextField> textfields = new ArrayList<JTextField>();
    ArrayList<JCheckBox> checkboxes = new ArrayList<JCheckBox>();
    Statement stmt = null;
    DatabaseConnector databaseConnector;
    Result(DatabaseConnector d){
        databaseConnector = d;
        JScrollPane sp = new JScrollPane(resultPanel);
        this.add(sp);

    }
    public void setQuery(ArrayList<QueryWithP> queries, int removed){
        /*
        for(int i = 0; i < queries.size(); i++){
            buttons.add(new JButton(queries.get(i).query));


        }
        */
        resultPanel.setLayout(new BoxLayout(resultPanel, BoxLayout.Y_AXIS));
        for(int i = 0; i < queries.size(); i++){
            checkboxes.add(new JCheckBox("["+ String.format("%.3f", queries.get(i).probability) + "] "+ queries.get(i).query));
        }
        for(JCheckBox cb : checkboxes){
            resultPanel.add(cb);
        }
        /*
        int i = 0;
        for(JTextField tf : textfields){
            bt.addActionListener(this);
            bt.setActionCommand(bt.getText());
            panelResult.add(bt);
            //panelResult.add(new JLabel(String.valueOf(queries.get(i).probability)));
            i++;

        }
        */
        JButton button = new JButton("Execute");
        button.addActionListener(this);
        resultPanel.add(new JLabel(String.valueOf(removed)+ " queries are not shown.                      " ));
        resultPanel.add(button);
        
        for (JCheckBox c : checkboxes) {
	        EventListener[] listeners = c.getListeners(MouseListener.class);
	        for (EventListener eventListener : listeners) {
	            c.removeMouseListener((MouseListener) eventListener);
	        }
	        
	        c.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mousePressed(MouseEvent e) {
	        		JCheckBox comp = (JCheckBox) e.getComponent();
	        		
	        		if ((e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK) {
	        			JFrame frame = new JFrame("Query Zoom");
	        			JPanel panel = new JPanel();
	        			JTextArea textarea = new JTextArea(comp.getText()); // 
	        			
	        		    panel.setLayout(new BorderLayout());
	        		    panel.setBorder(new EmptyBorder(10, 10, 10, 10));
	        		    
	        		    textarea.setLineWrap(true);
	        			textarea.setWrapStyleWord(true);
	        			textarea.setFont(textarea.getFont().deriveFont(16f));
	        			
	        			panel.add(textarea);
	        			frame.add(panel);
	        			
	        			
	        			frame.setSize(1000, 200);
	        			frame.setVisible(true);
	        			
	        			return;
	        		}
	        		
	        		
	        		if (comp.isSelected()) {
	        			comp.setSelected(false);
	        		}
	        		else {
	        			comp.setSelected(true);
	        		}
	        	}
	        });
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String sql = null;
        for(int a = 0 ; a < checkboxes.size() ;a++) {
            if(checkboxes.get(a).isSelected()) {
                sql= checkboxes.get(a).getText().substring(8);
                System.out.println(sql);
                int rowNum = 50;
                int colNum = 3;
                try {
                    stmt = databaseConnector.con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);


                    ResultSet rs;
                    //sql = "select * from Hotel_Reservation_View";
                    rs = stmt.executeQuery(sql);

                    //get row num
                    rs.last();
                    rowNum = rs.getRow();
                    rs.beforeFirst();

                    //get colnum and col names
                    ResultSetMetaData metaData = rs.getMetaData();
                    colNum = metaData.getColumnCount();
                    String columnName[] = new String[colNum];
                    for (int i = 1; i <= colNum; i++) {
                        columnName[i - 1] = metaData.getColumnLabel(i);
                    }
                    int n = 0;

                    String[][] rowDat = new String[rowNum][colNum];
                    while (rs.next()) {
                        for(int i = 1; i < colNum ;i++){
                            rowDat[n][i-1] = rs.getString(i);
                        }
                        n++;
                        //int id = rs.getInt("id");
                        //String name = rs.getString("name");
                        //System.out.println("ID：" + id);
                        //System.out.println("name：" + name);
                    }

                    DefaultTableModel tm = new DefaultTableModel(rowDat, columnName);
                    JTable tb = new JTable(tm);
                    tb.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    JScrollPane sp = new JScrollPane(tb);
                    sp.setSize(800,600);

                    JPanel panelResult = new JPanel();
                    Result result = new Result();
                    result.add(panelResult, BorderLayout.CENTER);

                    panelResult.add(sp);
                    result.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    result.setTitle(sql);
                    result.setSize(800, 600);
                    result.setVisible(true);



                } catch (SQLException es) {
                    Result result = new Result();
                    result.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    result.setTitle(sql);
                    result.add(new JLabel("Error on sql"));
                    result.setSize(800, 600);
                    result.setVisible(true);

                    es.printStackTrace();
                }
            }
            }
        }


}
