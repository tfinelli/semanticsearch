import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Generates random/semi-random data which can be used to quickly populate the database tables.
 * This eliminates the task of having to manually insert data for each of the tables.
 * Once the data is generated in memory it is written out to a file (specifically a tab separated spreadsheet format [.tsv]).
 * Each table gets its own .tsv file.  These .tsv files are then read by the DatabasePopulator class, which populates the database with the spreadsheet data.
 * 
 * The reason for why the data is written out to spreadsheet file first (rather than just inserting the data directly into the database) 
 * is because this method makes it very easy to manually add data by appending it to the spreadsheet.  For example, you may want some custom
 * data inserted (that is not randomly generated).  This custom data can simply be appended to the end of the .tsv file(s). 
 * 
 * @author Tom
 *
 */
public class DatabaseDataGenerator {
	
	static boolean append = false;
	static int rows = 15;
	
	/**
	 * Driver method which first reads in some necessary location, airport, and car data.  Then it calls each of the methods for
	 * generating random data.  These methods both generate the random data and then write it to .tsv files. 
	 * @throws IOException
	 */
	public static void generate_database_data() throws IOException {
		// Read in some location, airport, and car data from spreadsheets.
		// These are grouped related data, and they can't really be generated completely randomly, so we will use the files as a basis.
		Location.read_file();
		Airport.read_file();
		Car.read_file();
		
		// Customer
		customer();
		
		// Tour
		tour_company();
		tour_reservation();
		
		// Hotel
		room();
		hotel();
		hotel_reservation();
		
		// Flight
		airport();
		airline();
		flight();
		flight_reservation();
		
		// Car
		car();
		car_rental_company();
		car_reservation();
		
	}
	
	/**
	 * Generates random customer data and writes it to src/data/customer.tsv
	 * @throws IOException
	 */
	public static void customer() throws IOException {
		// String prefix = "INSERT INTO Customer(first_name, last_name, street_address, zip_code, state, country, phone_number) VALUES ("
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/customer.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_first_name() + "\t");
				writer.write(DataGenerator.generate_last_name() + "\t");
				writer.write(DataGenerator.generate_address() + "\t");
				Location loc = Location.get_random();
				writer.write(loc.zip_code + "\t");
				writer.write(loc.state + "\t");
				writer.write(loc.country + "\t");
				writer.write(DataGenerator.generate_phone_number());
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random tour_reservation data and writes it to src/data/tour_reservation.tsv
	 * @throws IOException
	 */
	public static void tour_reservation() throws IOException {
		// String prefix = "INSERT INTO Tour_Reservation(customer_id, tour_company_id, cost, date) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/tour_reservation.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_number(1, rows) + "\t");
				writer.write(DataGenerator.generate_number(1, rows) + "\t");
				writer.write(DataGenerator.generate_cost() + "\t");
				writer.write(DataGenerator.generate_date());
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random tour_company data and writes it to src/data/tour_company.tsv
	 * @throws IOException
	 */
	public static void tour_company() throws IOException {
		// String prefix = "INSERT INTO Tour_Company(name, street_address, city, zip_code, state, country, phone_number) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/tour_company.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_tour_company_name() + "\t");
				writer.write(DataGenerator.generate_address() + "\t");
				Location loc = Location.get_random();
				writer.write(loc.city + "\t");
				writer.write(loc.zip_code + "\t");
				writer.write(loc.state + "\t");
				writer.write(loc.country + "\t");
				writer.write(DataGenerator.generate_phone_number());
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random hotel data and writes it to src/data/hotel.tsv
	 * @throws IOException
	 */
	public static void hotel() throws IOException {
		// String prefix = "INSERT INTO Hotel(name, street_address, city, zip_code, state, country, phone_number, stars) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/hotel.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_hotel_company_name() + "\t");
				writer.write(DataGenerator.generate_address() + "\t");
				Location loc = Location.get_random();
				writer.write(loc.city + "\t");
				writer.write(loc.zip_code + "\t");
				writer.write(loc.state + "\t");
				writer.write(loc.country + "\t");
				writer.write(DataGenerator.generate_phone_number() + "\t");
				writer.write(Integer.toString(DataGenerator.generate_number(1, 5)));
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random room data and writes it to src/data/room.tsv
	 * @throws IOException
	 */
	public static void room() throws IOException {
		// String prefix = "INSERT INTO Room(number, type) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/room.tsv", append), "utf-8"));
		
		for (int i = 1; i <= 3; i++ ) {
				writer.write(i + "\t");
				writer.write(i + " beds");
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random hotel_reservation data and writes it to src/data/hotel_reservation.tsv
	 * @throws IOException
	 */
	public static void hotel_reservation() throws IOException {
		// String prefix = "INSERT INTO Hotel_Reservation(room_id, customer_id, hotel_id, date, duration, cost) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/hotel_reservation.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_number(1, 3) + "\t");
				writer.write(DataGenerator.generate_number(1, rows) + "\t");
				writer.write(DataGenerator.generate_number(1, rows) + "\t");
				writer.write(DataGenerator.generate_date() + "\t");
				writer.write(DataGenerator.generate_number(1, 4) + "\t");
				writer.write(Integer.toString(DataGenerator.generate_cost()));
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random airport data and writes it to src/data/airport.tsv
	 * @throws IOException
	 */
	public static void airport() throws IOException {
		// String prefix = "INSERT INTO Airport(name, code, street_address, city, zip_code, state, country, phone_number) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/airport.tsv", append), "utf-8"));
		
		for (int i = 0; i < Airport.airports.size(); i++ ) {
				Airport airport = Airport.airports.get(i);
				writer.write(airport.name + "\t");
				writer.write(airport.code + "\t");
				writer.write(DataGenerator.generate_address() + "\t");
				writer.write(airport.city + "\t");
				writer.write(airport.zip_code + "\t");
				writer.write(airport.state + "\t");
				writer.write(airport.country + "\t");
				writer.write(DataGenerator.generate_phone_number());
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random airline data and writes it to src/data/airline.tsv
	 * @throws IOException
	 */
	public static void airline() throws IOException {
		// String prefix = "INSERT INTO Airline(name, street_address, city, state, zip_code, country, phone_number) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/airline.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_airline_name() + "\t");
				writer.write(DataGenerator.generate_address() + "\t");
				Location loc = Location.get_random();
				writer.write(loc.city + "\t");
				writer.write(loc.zip_code + "\t");
				writer.write(loc.state + "\t");
				writer.write(loc.country + "\t");
				writer.write(DataGenerator.generate_phone_number());
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random flight data and writes it to src/data/flight.tsv
	 * @throws IOException
	 */
	public static void flight() throws IOException {
		// String prefix = "INSERT INTO Flight(source_airport_id, destination_airport_id, airline_id, cost, duration, date) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/flight.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_number(1, Airport.airports.size()) + "\t");
				writer.write(DataGenerator.generate_number(1, Airport.airports.size()) + "\t");
				writer.write(DataGenerator.generate_number(1, rows) + "\t");
				writer.write(DataGenerator.generate_cost() + "\t");
				writer.write(DataGenerator.generate_number(1, 24) + "\t");
				writer.write(DataGenerator.generate_date());
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random flight_reservation data and writes it to src/data/flight_reservation.tsv
	 * @throws IOException
	 */
	public static void flight_reservation() throws IOException {
		// String prefix = "INSERT INTO Flight_Reservation(flight_id, customer_id) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/flight_reservation.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_number(1, rows) + "\t");
				writer.write(Integer.toString(DataGenerator.generate_number(1, rows)));
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random car_rental_company data and writes it to src/data/car_rental_company.tsv
	 * @throws IOException
	 */
	public static void car_rental_company() throws IOException {
		// String prefix = "INSERT INTO Car_Rental_Company(name, street_address, city, state, zip_code, country, phone_number) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/car_rental_company.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_car_rental_company_name() + "\t");
				writer.write(DataGenerator.generate_address() + "\t");
				Location loc = Location.get_random();
				writer.write(loc.city + "\t");
				writer.write(loc.state + "\t");
				writer.write(loc.zip_code + "\t");
				writer.write(loc.country + "\t");
				writer.write(DataGenerator.generate_phone_number());
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random car data and writes it to src/data/car.tsv
	 * @throws IOException
	 */
	public static void car() throws IOException {
		// String prefix = "INSERT INTO Car(make, model, year, color, mpg) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/car.tsv", append), "utf-8"));
		
		for (int i = 0; i < Car.cars.size(); i++ ) {
				Car car = Car.cars.get(i);
				writer.write(car.make + "\t");
				writer.write(car.model + "\t");
				writer.write(DataGenerator.generate_number(2000, 2016) + "\t");
				writer.write(DataGenerator.generate_color() + "\t");
				writer.write(Integer.toString(DataGenerator.generate_number(20, 35)));
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	/**
	 * Generates random car_reservation data and writes it to src/data/car_reservation.tsv
	 * @throws IOException
	 */
	public static void car_reservation() throws IOException {
		// String prefix = "INSERT INTO Car_Reservation(car_id, customer_id, car_rental_company_id, date, duration, cost) VALUES (";
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/data/car_reservation.tsv", append), "utf-8"));
		
		for (int i = 0; i < rows; i++ ) {
				writer.write(DataGenerator.generate_number(1, Car.cars.size()) + "\t");
				writer.write(DataGenerator.generate_number(1, rows) + "\t");
				writer.write(DataGenerator.generate_number(1, rows) + "\t");
				writer.write(DataGenerator.generate_date() + "\t");
				writer.write(DataGenerator.generate_number(1, 4) + "\t");
				writer.write(Integer.toString(DataGenerator.generate_cost()));
				writer.write("\n");
		}
		
		writer.close();
		
	}
	
	

}
