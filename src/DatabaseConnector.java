import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.*;

/**
 * Class for connecting to the database
 *
 */
public class DatabaseConnector extends JFrame implements ActionListener {
    JLabel label_fn;
    String driver = "org.postgresql.Driver";
    String server = "";
    String dbname   = "";
    String url;
    String user     = "";
    String password = "";
    JPanel panel;
    JTextField textFieldServer;
    JTextField textFieldUser;
    JTextField textFieldPassword;
    JTextField textFieldDatabase;
    JButton button;
    JLabel label;
    Connection con;

    /**
     * Constructor
     *
     */
    DatabaseConnector(){
        textFieldServer = new JTextField(20);
        textFieldUser = new JTextField(20);
        textFieldPassword = new JTextField(20);
        textFieldDatabase = new JTextField(20);
        textFieldServer.setText("Input server address here");
        textFieldUser.setText("Input user name here");
        textFieldPassword.setText("Input password here");
        textFieldDatabase.setText("Input database name here");
        textFieldServer.addFocusListener(new PlaceholderFocusListener(textFieldServer));
        textFieldUser.addFocusListener(new PlaceholderFocusListener(textFieldUser));
        textFieldPassword.addFocusListener(new PlaceholderFocusListener(textFieldPassword));
        textFieldDatabase.addFocusListener(new PlaceholderFocusListener(textFieldDatabase));
        button = new JButton("Connect");
        label = new JLabel("Successfully connected to the database.");
        panel = new JPanel();

        button.addActionListener(this);
        button.setActionCommand("CONNECT");

        panel.add(textFieldServer);
        panel.add(textFieldUser);
        panel.add(textFieldPassword);
        panel.add(textFieldDatabase);
        panel.add(button);
        panel.add(label);
        label.setVisible(false);

        this.add(panel);

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("CONNECT")){
            server = textFieldServer.getText();
            user = textFieldUser.getText();
            password = textFieldPassword.getText();
            dbname = textFieldDatabase.getText();
            String url = "jdbc:postgresql://" + server + "/" + dbname;
            System.out.println("connecting to "+ url);

            try {
                ////for debug
                url = "jdbc:postgresql://localhost/semanticsearch";
                user = "postgres";
                password = "root";

                ////////

                Class.forName (driver);
                con = DriverManager.getConnection(url, user, password);
                System.out.println("connecting");


                label.setText("Successfully connected to the database.");
                label.setForeground(Color.GREEN);
                label.setVisible(true);
                button.setText("Next");
                button.setActionCommand("OK_DB");
                //rs.close();
                //stmt.close();
                //con.close();

            } catch (ClassNotFoundException e1) {
                System.out.println("error1");
                e1.printStackTrace();
            } catch (SQLException e1) {
                label.setText("Failed to connect to the database. Try again");
                label.setForeground(Color.RED);
                label.setVisible(true);
                System.out.println("error2");
                e1.printStackTrace();
            }



        }
        else if (e.getActionCommand().equals("OK_DB")){
            FileChooser fileChooser = new FileChooser(this);

            fileChooser.setDefaultCloseOperation(EXIT_ON_CLOSE);
            fileChooser.setSize(300, 200);
            fileChooser.setLocationRelativeTo(null);
            fileChooser.setTitle("Open File");
            fileChooser.setVisible(true);
            this.setVisible(false);
        }


    }
}

class PlaceholderFocusListener implements FocusListener {
    private static final Color INACTIVE = UIManager.getColor("TextField.inactiveForeground");
    private final String hintMessage;
    public PlaceholderFocusListener(JTextComponent tf) {
        hintMessage = tf.getText();
        tf.setForeground(INACTIVE);
    }
    @Override public void focusGained(FocusEvent e) {
        JTextComponent tf = (JTextComponent)e.getSource();
        if(hintMessage.equals(tf.getText()) && INACTIVE.equals(tf.getForeground())) {
            tf.setForeground(UIManager.getColor("TextField.foreground"));
            tf.setText("");
        }
    }
    @Override public void focusLost(FocusEvent e) {
        JTextComponent tf = (JTextComponent)e.getSource();
        if("".equals(tf.getText().trim())) {
            tf.setForeground(INACTIVE);
            tf.setText(hintMessage);
        }
    }
}