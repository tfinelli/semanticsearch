import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Contains JUnit tests for the tool
 * @author Tom
 *
 */
public class Test {

	/**
	 * Test the random number generation method
	 */
	@org.junit.Test
	public void test_generate_number() {
		
		for (int i = 0; i < 5; i++) {
			int number = DataGenerator.generate_number(0, 10);
			
			assertTrue(number >= 0);
			assertTrue(number <= 10);
			System.out.println(number);
		}
		
		for (int i = 0; i < 5; i++) {
			int number = DataGenerator.generate_number(1000, 9999);
			assertTrue(number >= 1000);
			assertTrue(number <= 9999);
			System.out.println(number);
		}
		
	}
	
	/**
	 * Test the random phone number generator method
	 */
	@org.junit.Test
	public void test_generate_phone_number() {
		System.out.println(DataGenerator.generate_phone_number());
		System.out.println(DataGenerator.generate_phone_number());
		System.out.println(DataGenerator.generate_phone_number());
	}

	/**
	 * Print/test the random address generator method
	 */
	@org.junit.Test
	public void test_generate_address() {
		for (int i = 0; i < 5; i++) {
			System.out.println(DataGenerator.generate_address());
		}
	}
	
	/**
	 * Test and print the creation of a mapping
	 */
	@org.junit.Test	
	public void test_mapping() {
		ArrayList<String> tables = new ArrayList<String>();
		tables.add("Hotel_Reservation_View");
		String expression = "*.city = 'boston'";
		
		Mapping mapping = new Mapping();
		mapping.add_tables(tables);
		mapping.add_expression(expression);
		
		System.out.println(mapping.generate_sql());
		
	}
	
	/**
	 * Test the Location class to make sure it correctly instantiates each row of location data as a Location object
	 */
	@org.junit.Test	
	public void test_location() {
		Location.read_file();
		
		Location loc = Location.locations.get(0);
		//assertNull(loc.state);
		//assertNull(loc.zip_code);
		assertEquals(loc.country, "nigeria");
		assertEquals(loc.city, "lagos");
		
	}
	
	/**
	 * Test the DatabaseDataGenerator's ability to generate and write out the tour_reservation data to a file (i.e. tour_reservation.tsv)
	 */
	@org.junit.Test	
	public void test_writer() {
		Location.read_file();
		
		try {
			DatabaseDataGenerator.tour_reservation();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	
	/**
	 * Test/print name the current working directory
	 */
	@org.junit.Test
	public void test_pwd() {
		System.out.println(System.getProperty("user.dir"));
	}
}
