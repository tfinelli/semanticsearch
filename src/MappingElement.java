import java.util.ArrayList;

/**
 * Created by rchwhm on 12/6/16.
 */
public class MappingElement {
    String name;
    ArrayList<String> tables;
    String expression;
    double probabilities;
    MappingElement(){
        name = null;
        tables = null;
        expression = null;
        probabilities = -1;
    }
    MappingElement(String name_, ArrayList<String> list, String exp, double prob){
        name = name_;
        tables = list;
        expression = exp;
        probabilities = prob;
    }
}
