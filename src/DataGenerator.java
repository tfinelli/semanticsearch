import java.util.ArrayList;
import java.util.Random;

public class DataGenerator {
	
	public static final Random rand = new Random(0);
	static int LO = 0;
	static int ME = 1;
	static int HI = 2;
	
	public static String generate_phone_number() {
		String result;
		result = Integer.toString(generate_number(200, 999));
		result += "-";
		result += Integer.toString(generate_number(200, 999));
		result += "-";
		result += Integer.toString(generate_number(1000, 9999));
		
		return result;
	}
	
	public static String generate_address() {
		String result;
		ArrayList<String> streets = new ArrayList<String>();
		streets.add("Main");
		streets.add("Grove");
		streets.add("Birch");
		streets.add("West");
		streets.add("North");
		streets.add("South");
		streets.add("East");
		streets.add("Oak");
		streets.add("Pine");
		streets.add("Maple");
		streets.add("Cedar");
		streets.add("Elm");
		
		ArrayList<String> suffixes = new ArrayList<String>();
		suffixes.add("Street");
		suffixes.add("Drive");
		suffixes.add("Circle");
		suffixes.add("Road");
		suffixes.add("Crossing");
		suffixes.add("Lane");
		
				
		result = Integer.toString(generate_number(1, 500));
		result += " ";
		result += streets.get(generate_number(0, streets.size() - 1));
		result += " ";
		result += suffixes.get(generate_number(0, suffixes.size() - 1));
		
		return result;		
	}
	
	public static int generate_number(int min, int max) {
		
		return rand.nextInt(max - min + 1) + min;
	}
	
	public static String generate_date() {
		String year = Integer.toString(generate_number(2000, 2016));
		String month = Integer.toString(generate_number(1, 12));
		String day = Integer.toString(generate_number(1, 28));
		
		return year + "-" + month + "-" + day;
	}
	
	
	public static String generate_first_name() {
		ArrayList<String> names = new ArrayList<String>();
		names.add("Barbara");
		names.add("Jane");
		names.add("John");
		names.add("Bob");
		names.add("Charlie");
		names.add("Robert");
		names.add("Richard");
		names.add("Susan");
		
		return names.get(generate_number(0, names.size() - 1));
	}
	
	public static String generate_last_name() {
		ArrayList<String> names = new ArrayList<String>();
		names.add("Smith");
		names.add("Green");
		names.add("Williams");
		names.add("Jones");
		names.add("Miller");
		names.add("Davis");
		names.add("Harris");
		names.add("Jackson");
		names.add("Clark");
		
		return names.get(generate_number(0, names.size() - 1));
	}
	
	public static int generate_cost(int type) {
		
		if (type == LO)
			return generate_number(25, 300);
		else if (type == ME) {
			return generate_number(300, 1000);
		}
		else if (type == HI) {
			return generate_number(1000, 3000);
		}
		else {
			return 0;
		}
	}
	
	public static int generate_cost() {
		return generate_cost(LO);
	}

	
	public static String generate_tour_company_name() {
		ArrayList<String> names = new ArrayList<String>();
		names.add("Temperate");
		names.add("Sunny");
		names.add("Historic");
		names.add("Authentic");
		names.add("Lasting");
		names.add("Memorable");
		names.add("Quick");
		names.add("Local");
		names.add("City");
		
		return names.get(generate_number(0, names.size() - 1)) + " Tours";
	}
	
	
	public static String generate_hotel_company_name() {
		ArrayList<String> names = new ArrayList<String>();
		names.add("Grand");
		names.add("Palazzo");
		names.add("Comfort");
		names.add("International");
		names.add("Super");
		names.add("Signature");
		names.add("King");
		names.add("Duke");
		names.add("Homestead");
		
		return names.get(generate_number(0, names.size() - 1)) + " Hotel";
	}
	
	public static String generate_car_rental_company_name() {
		ArrayList<String> names = new ArrayList<String>();
		names.add("Speedy");
		names.add("Fast");
		names.add("Quick");
		names.add("Efficient");
		names.add("Electric");
		names.add("Sport");
		names.add("Safety");
		names.add("Cargo");
		names.add("Lux");
		
		return names.get(generate_number(0, names.size() - 1)) + " Car Rental";
	}
	
	public static String generate_airline_name() {
		ArrayList<String> names = new ArrayList<String>();
		names.add("Gliding");
		names.add("Soars");
		names.add("Float");
		names.add("Buoyant");
		names.add("Levitating");
		names.add("Drag");
		names.add("Lift");
		names.add("Flow");
		names.add("Transient");
		
		return names.get(generate_number(0, names.size() - 1)) + " Airlines";
	}
	
	public static String generate_color() {
		ArrayList<String> names = new ArrayList<String>();
		names.add("black");
		names.add("white");
		names.add("gray");
		names.add("blue");
		names.add("red");
		names.add("yellow");
		
		return names.get(generate_number(0, names.size() - 1));
	}
}
