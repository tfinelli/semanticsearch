import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @deprecated
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * WARNING: This class is deprecated.  It was initially used for results that involved only a single table.
 * It has been replaced with a different class which is capable of handling multiple table and single table results.
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * 
 * Represents an ontology-to-database mapping which has a set of tables and an expression.
 * @author Tom
 *
 */
public class Mapping {
	
	public HashMap<String, Integer> probability;
	public String expression;
	public ArrayList<String> expression_list = new ArrayList<String>();
	
	/**
	 * Constructor for a mapping
	 */
	public Mapping() {
		this.probability = new HashMap<String, Integer>();
		this.expression = null;
	}
	
	/**
	 * Transforms a list of tables into a hashmap of tables with each key being the table name and
	 * the value being the frequency of the table name in the list of tables (i.e. the number of 
	 * times that the table name appeared in the list of tables).
	 * @param tables
	 */
	public void add_tables(ArrayList<String> tables) {
		for (String table : tables) {
			if (table == null) {
				continue;
			}
			if (this.probability.containsKey(table)) {  // There is already an entry in the hashmap for this table name, so increment it
				this.probability.put(table, this.probability.get(table) + 1);
			}
			else {
				this.probability.put(table, 1);  // There is no entry in the hashmap for this table name, so add it with a value of 1
			}
		}
		
	}
	
	/**
	 * @deprecated
	 * WARNING: This method has been replaced with the add_expression_list() method which can handle multiple expressions.
	 * Sets the expression.
	 * @param expression
	 */
	public void add_expression(String expression) {
		this.expression = expression.replace('"', '\'');
	}
	
	/**
	 * Copies the specified expression_list into the class defined expression_list
	 * @param expression_list
	 */
	public void add_expression_list(ArrayList<String> expression_list) {
		this.expression_list = new ArrayList<String>(expression_list);
		for (int i = 0; i < this.expression_list.size(); i++) {
			this.expression_list.set(i, this.expression_list.get(i).replace('"', '\''));
		}
	}
	
	/**
	 * Determines the table probability through table frequency 
	 * @return the name of the table with the highest frequency (i.e. probability)
	 */
	public String probability() {
		String highest_table = null;
		Integer highest_table_probability = 0;
		for (Map.Entry<String, Integer> entry : this.probability.entrySet()) {
		    String key = entry.getKey();
		    Integer value = entry.getValue();
		    
		    if (highest_table_probability < value) {
		    	highest_table_probability = value;
		    	highest_table = key;
		    }
		    
		}
		
		return highest_table;
	}
	
	/**
	 * @deprecated
	 * WARNING: This method has been replaced with the generate_sql_with_expression_list() method which can handle multiple expressions.
	 * Generates the SQL statement using the table with the highest frequency/probability and its corresponding expression.
	 * @return a valid SQL statement which can be executed on the database
	 */
	public String generate_sql() {
		String sql = "SELECT * FROM " + probability() + " ";
		
		if (this.expression != null) {
			String working_expression = expression;
			if (probability().equals("Flight_Reservation_View")) {
				working_expression = working_expression.replace("state", "destination_state").replace("city", "destination_city").replace("country", "destination_country");
			}
			
			sql += "WHERE " + working_expression.replace("*", probability());
		}
		
		return sql;
	}
	
	/**
	 * Generates a SQL statement using the table with the highest frequency/probability and *ALL* of the expressions listed in the list of expressions.
	 * @return a valid SQL statement which can be executed on the database
	 */
	public String generate_sql_with_expression_list() {
		String sql = "SELECT * FROM " + probability() + " ";
		
		if (!this.expression_list.isEmpty()) {
			
			// Apply fix for flight table
			/*
			if (probability().equals("Flight_Reservation_View")) {
				for (int i = 0; i < expression_list.size(); i++) {
					expression_list.set(i, expression_list.get(i).replace("state", "destination_state").replace("city", "destination_city").replace("country", "destination_country")) ;
				}
			}
			*/
			
			sql += "WHERE ";
		}
		for (int i = 0; i < expression_list.size(); i++) {
			if (i != 0) {
				sql += " AND "; // append AND first
			}
			sql += expression_list.get(i).replace("*", probability());
		}
		
		
		return sql;
		
	}
	
	

}
