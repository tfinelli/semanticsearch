import java.util.ArrayList;

/**
 * Created by rchwhm on 11/9/16.
 */
public class Ontology {
    ArrayList<String> names;
    ArrayList<Ontology> children = new ArrayList<Ontology>();
    Ontology searchResult = null;

    Ontology(ArrayList<String> str){
        this.names = str;
    }
    public void addNode(Ontology node){
        this.children.add(node);
    }
    public void printOntology(){

        System.out.println(names + " size" +this.children.size() );
        for(Ontology o : this.children){
            o.printOntology();
        }

    }
    public Ontology searchString(String str){
        Ontology temp;
        for(String s : this.names){
            //System.out.println("comparing "+ s +" and " + str);
            if(s.equals(str)){

                return this;
            }
            //System.out.println("result:"+ searchResult);
        }
        for(Ontology o : this.children){
             temp = o.searchString(str);
            if(temp != null) return temp;
        }
        return null;
    }
}
