import java.sql.SQLException;
import java.sql.Statement;

/**
 * Generates/Creates each of the tables and views for the database.
 * 
 * NOTE: Does not insert any data into any of the tables.  Data insertion is handled by the DatabasePopular class.
 * 
 * @author Tom
 *
 */
public class DatabaseGenerator {
	
	/**
	 * Driver method which calls each of the table generating/creating methods.
	 * @param st
	 * @throws SQLException
	 */
	public static void generate_database(Statement st) throws SQLException {		
		// Customer
		customer(st);
		
		// Tour
		//tour(st);
		tour_company(st);
		tour_reservation(st);
		
		// Hotel
		//room_type(st);
		room(st);
		hotel(st);
		hotel_reservation(st);
		
		// Flight
		airport(st);
		airline(st);
		flight(st);
		flight_reservation(st);
		
		// Car
		car(st);
		car_rental_company(st);
		car_reservation(st);
		
		
		// Create Views
		create_views(st);
			
		
	}
	

	/**
	 * Generates customer table.
	 * @param st
	 * @throws SQLException
	 */
	public static void customer(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Customer CASCADE");
		
		st.execute("CREATE TABLE Customer ("
				+ "id serial PRIMARY KEY,"
				+ "first_name varchar(50),"
				+ "last_name varchar(50),"
				+ "street_address varchar(100),"
				+ "zip_code varchar(10),"
				+ "state varchar(50),"
				+ "country varchar(100),"
				+ "phone_number varchar (15)"
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Customer(first_name, last_name, street_address, zip_code, state, country, phone_number) VALUES (";
		
		st.execute(prefix + "'Bob', 'Smith', '100 Institute Road', '01609', 'MA', 'USA', '555-555-1234'" + ")");
		st.execute(prefix + "'Jane', 'Smith', '102 Institute Road', '01609', 'MA', 'USA', '555-555-1235'" + ")");
		st.execute(prefix + "'Barbara', 'Green', '24 Round Way', '01234', 'ME', 'USA', '734-555-1234'" + ")");
		*/
	}
	
	
	/**
	 * Creates tour_reservation_table
	 * @param st
	 * @throws SQLException
	 */
	public static void tour_reservation(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Tour_Reservation CASCADE");
		
		st.execute("CREATE TABLE Tour_Reservation ("
				+ "id serial PRIMARY KEY,"
				+ "customer_id serial REFERENCES Customer,"
				+ "tour_company_id serial REFERENCES Tour_Company,"
				+ "cost int,"
				+ "date date"
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Tour_Reservation(customer_id, tour_company_id, cost, date) VALUES (";
		
		st.execute(prefix + "2, 3, 85, '2016-11-01'" + ")");
		*/
	}
	
	/**
	 * Creates tour_company table
	 * @param st
	 * @throws SQLException
	 */
	public static void tour_company(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Tour_Company CASCADE");
		
		st.execute("CREATE TABLE Tour_Company ("
				+ "id serial PRIMARY KEY,"
				+ "name citext,"			// 200
				+ "street_address citext,"	// 100
				+ "city citext,"			// 100
				+ "zip_code citext,"		// 10
				+ "state citext,"			// 50
				+ "country citext,"			// 100
				+ "phone_number citext"		// 15
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Tour_Company(name, street_address, city, zip_code, state, country, phone_number) VALUES (";
		
		st.execute(prefix + "'Massachusetts Tours', '20 Main Street', 'Worcester', '01609', 'MA', 'USA', '777-512-4032'" + ")");
		st.execute(prefix + "'World Tours', '53 Canal Street', 'Boston', '04509', 'MA', 'USA', '245-815-3999'" + ")");
		st.execute(prefix + "'International Tours', '3 Oak Street', 'New York City', '01000', 'NY', 'USA', '413-700-1555'" + ")");
		*/
	}
	
	/**
	 * Creates hotel table
	 * @param st
	 * @throws SQLException
	 */
	public static void hotel(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Hotel CASCADE");
		
		st.execute("CREATE TABLE Hotel ("
				+ "id serial PRIMARY KEY,"
				+ "name citext,"			// 200
				+ "street_address citext,"	//100
				+ "city citext,"			// 100
				+ "zip_code citext,"		// 10
				+ "state citext,"			// 50
				+ "country citext,"			// 100
				+ "phone_number citext,"	// 15
				+ "stars int"
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Hotel(name, street_address, city, zip_code, state, country, phone_number, stars) VALUES (";
		
		st.execute(prefix + "'Grand Hotel', '5 West Street', 'Worcester', '01609', 'MA', 'USA', '555-345-5000', 4" + ")");
		st.execute(prefix + "'Travelers Hotel', '47 East Street', 'Florence', NULL, NULL, 'Italy', '333-345-5000', 4" + ")");
		st.execute(prefix + "'Beau Hotel', '10 Park Street', 'Paris', NULL, NULL, 'France', '888-345-5000', 5" + ")");
		*/
	}
	
	/**
	 * Creates room table
	 * @param st
	 * @throws SQLException
	 */
	public static void room(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Room CASCADE");
		
		st.execute("CREATE TABLE Room ("
				+ "id serial PRIMARY KEY,"
				+ "number int,"
				+ "type varchar(100)"
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Room(number, type) VALUES (";
		
		st.execute(prefix + "1, '2 beds'" + ")");
		st.execute(prefix + "2, '1 bed'" + ")");
		st.execute(prefix + "3, '1 bed'" + ")");
		*/
	}
	
	/**
	 * Creates hotel_reservation table
	 * @param st
	 * @throws SQLException
	 */
	public static void hotel_reservation(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Hotel_Reservation CASCADE");
		
		st.execute("CREATE TABLE Hotel_Reservation ("
				+ "id serial PRIMARY KEY,"
				+ "room_id serial REFERENCES Room,"
				+ "customer_id serial REFERENCES Customer,"
				+ "hotel_id serial REFERENCES Hotel,"
				+ "date date,"
				+ "duration int,"
				+ "cost int"
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Hotel_Reservation(room_id, customer_id, hotel_id, date, duration, cost) VALUES (";
		
		st.execute(prefix + "1, 1, 1, '2016-01-01', 3, 1000" + ")");
		st.execute(prefix + "1, 1, 1, '2016-04-10', 1, 200" + ")");
		st.execute(prefix + "1, 1, 1, '2016-09-15', 1, 400" + ")");
		*/
	}
	
	/**
	 * Creates airport table
	 * @param st
	 * @throws SQLException
	 */
	public static void airport(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Airport CASCADE");
		
		st.execute("CREATE TABLE Airport ("
				+ "id serial PRIMARY KEY,"
				+ "name citext,"			// 200
				+ "code citext,"			// 5
				+ "street_address citext,"	// 100
				+ "city citext,"			// 100
				+ "zip_code citext,"		// 10
				+ "state citext,"			// 50
				+ "country citext,"			// 100
				+ "phone_number citext"		// 15
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Airport(name, code, street_address, city, zip_code, state, country, phone_number) VALUES (";
		
		st.execute(prefix + "'LaGuardia', 'LGA', '2 Main Street', 'New York City', '11371', 'NY', 'USA', '540-545-5000'" + ")");
		st.execute(prefix + "'Indira Gandhi International Airport', 'DEL', '2 Vista Lane', 'Delhi', NULL, NULL, 'India', '888-645-5000'" + ")");
		st.execute(prefix + "'Frankfurt Airport', 'FRA', '8 Flight Street', 'Frankfurt', NULL, NULL, 'Germany', '777-823-4000'" + ")");
		st.execute(prefix + "'London Heathrow', 'LHR', '12 Booth Street', 'London', NULL, NULL, 'United Kingdom', '577-223-8900'" + ")");
		st.execute(prefix + "'Toronto Pearson International', 'YYZ', '54 Cold Street', 'Toronto', NULL, NULL, 'Canada', '747-843-7000'" + ")");
		*/
	}
	
	/**
	 * Creates airline table
	 * @param st
	 * @throws SQLException
	 */
	public static void airline(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Airline CASCADE");
		
		st.execute("CREATE TABLE Airline ("
				+ "id serial PRIMARY KEY,"
				+ "name citext,"			// 200
				+ "street_address citext,"	// 100
				+ "city citext,"			// 100
				+ "state citext,"			// 50
				+ "zip_code citext,"		// 10
				+ "country citext,"			// 100
				+ "phone_number citext"		// 15
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Airline(name, street_address, city, state, zip_code, country, phone_number) VALUES (";
		
		st.execute(prefix + "'Delta', '10 South Street', 'Atlanta', 'GA', '01511', 'USA', '588-412-5000'" + ")");
		st.execute(prefix + "'American Airlines', '84 Burrow Lane', 'Fort Worth', 'TX', '04244', 'USA', '642-311-5000'" + ")");
		st.execute(prefix + "'Alitalia', '10 Otto Road', 'Rome', NULL, NULL, 'Italy', '321-645-4000'" + ")");
		*/
	}
	
	/**
	 * Creates flight table
	 * @param st
	 * @throws SQLException
	 */
	public static void flight(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Flight CASCADE");
		
		st.execute("CREATE TABLE Flight ("
				+ "id serial PRIMARY KEY,"
				+ "source_airport_id serial REFERENCES Airport,"
				+ "destination_airport_id serial REFERENCES Airport,"
				+ "airline_id serial REFERENCES Airline,"
				+ "cost int,"
				+ "duration int,"
				+ "date timestamp"
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Flight(source_airport_id, destination_airport_id, airline_id, cost, duration, date) VALUES (";
		
		st.execute(prefix + "1, 2, 1, 600, 10, '2016-10-01 10:00'" + ")");
		st.execute(prefix + "2, 3, 2, 500, 9, '2016-04-01 01:00'" + ")");
		st.execute(prefix + "1, 3, 3, 400, 8, '2016-05-01 02:00'" + ")");
		st.execute(prefix + "4, 5, 2, 450, 9, '2016-03-01 01:00'" + ")"); // London to Toronto
		st.execute(prefix + "2, 5, 3, 725, 8, '2016-02-01 02:00'" + ")"); // India to Toronto
		*/
	}
	
	/**
	 * Creates flight_reservation table
	 * @param st
	 * @throws SQLException
	 */
	public static void flight_reservation(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Flight_Reservation CASCADE");
		
		st.execute("CREATE TABLE Flight_Reservation ("
				+ "id serial PRIMARY KEY,"
				+ "flight_id serial REFERENCES Flight,"
				+ "customer_id serial REFERENCES Customer"
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Flight_Reservation(flight_id, customer_id) VALUES (";
		
		st.execute(prefix + "1, 1" + ")");
		st.execute(prefix + "2, 2" + ")");
		st.execute(prefix + "3, 3" + ")");
		st.execute(prefix + "4, 3" + ")");
		st.execute(prefix + "5, 3" + ")");
		*/
	}
	
	/**
	 * Creates car_rental_company table
	 * @param st
	 * @throws SQLException
	 */
	public static void car_rental_company(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Car_Rental_Company CASCADE");
		
		st.execute("CREATE TABLE Car_Rental_Company ("
				+ "id serial PRIMARY KEY,"
				+ "name citext,"			// 200
				+ "street_address citext,"	// 100
				+ "city citext,"			// 100
				+ "state citext,"			// 50
				+ "zip_code citext,"		// 10
				+ "country citext,"			// 100
				+ "phone_number citext"		// 15
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Car_Rental_Company(name, street_address, city, state, zip_code, country, phone_number) VALUES (";
		
		st.execute(prefix + "'Greenwich Village', '221 Thompson Street', 'New York City', 'NY', '10012', 'USA', '212-533-2900'" + ")");
		st.execute(prefix + "'Savaari', '40 Main Street', 'Delhi', NULL, NULL, 'India', '700-423-8000'" + ")"); // name is company name & address and phone is made up
		st.execute(prefix + "'Enterprise Frankfurt Terminal', '8 Flight Street', 'Frankfurt', NULL, NULL, 'Germany', '848-210-8900'" + ")");
		*/
	}
	
	/**
	 * Creates car table
	 * @param st
	 * @throws SQLException
	 */
	public static void car(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Car CASCADE");
		
		st.execute("CREATE TABLE Car ("
				+ "id serial PRIMARY KEY,"
				+ "make varchar(100),"
				+ "model varchar(100),"
				+ "year int,"
				+ "color varchar(50),"
				+ "mpg int"
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Car(make, model, year, color, mpg) VALUES (";
		
		st.execute(prefix + "'Honda', 'Accord', 2014, 'white', 32" + ")");
		st.execute(prefix + "'Hyundai', 'Tuscon', 2015, 'gray', 29" + ")");
		st.execute(prefix + "'Toyota', 'Camry', 2013, 'black', 33" + ")");
		*/
	}
	
	/**
	 * Creates car_reservation table
	 * @param st
	 * @throws SQLException
	 */
	public static void car_reservation(Statement st) throws SQLException {
		st.execute("DROP TABLE IF EXISTS Car_Reservation CASCADE");
		
		st.execute("CREATE TABLE Car_Reservation ("
				+ "id serial PRIMARY KEY,"
				+ "car_id serial REFERENCES Car,"
				+ "customer_id serial REFERENCES Customer,"
				+ "car_rental_company_id serial REFERENCES Car_Rental_Company,"
				+ "date date,"
				+ "duration int,"
				+ "cost int"
				+ ")"
				);
		
		/*
		String prefix = "INSERT INTO Car_Reservation(car_id, customer_id, car_rental_company_id, date, duration, cost) VALUES (";
		
		st.execute(prefix + "1, 1, 1, '2016-01-10', 2, 200" + ")");
		st.execute(prefix + "1, 2, 3, '2016-05-10', 3, 300" + ")");
		st.execute(prefix + "3, 3, 2, '2016-09-20', 1, 100" + ")");
		*/
		
	}
	
	/**
	 * Main driver method for creating all of the views
	 * @param st
	 * @throws SQLException
	 */
	public static void create_views(Statement st) throws SQLException {
		flight_airport_view(st);
		flight_reservation_view(st);
		car_reservation_view(st);
		hotel_reservation_view(st);
		tour_reservation_view(st);
	}
	
	/**
	 * Creates flight_airport_view
	 * @param st
	 * @throws SQLException
	 */
	public static void flight_airport_view(Statement st) throws SQLException {
		st.execute("DROP VIEW IF EXISTS Flight_Airport_View CASCADE");
		
		String create_view = "CREATE VIEW Flight_Airport_View AS "
				+ "SELECT Flight.id, Flight.airline_id, Flight.cost, Flight.duration, "
				+ "Flight.date, Airport_Source.city AS source_city, Airport_Source.state AS source_state, "
				+ "Airport_Source.country AS source_country, Airport_Destination.city AS city, "
				+ "Airport_Destination.state AS state, Airport_Destination.country AS country "
				+ "FROM Flight, Airport AS Airport_Source, Airport AS Airport_Destination "
				+ "WHERE Airport_Source.id = Flight.source_airport_id AND Airport_Destination.id = Flight.destination_airport_id";
		
		//System.out.println(create_view.substring(359));
		
		st.execute(create_view);
	}
	
	/**
	 * Creates flight_reservation_view
	 * @param st
	 * @throws SQLException
	 */
	public static void flight_reservation_view(Statement st) throws SQLException {
		st.execute("DROP VIEW IF EXISTS Flight_Reservation_View CASCADE");
		
		
		String create_view = "CREATE VIEW Flight_Reservation_View AS "
				+ "SELECT fav.airline_id, fav.cost, fav.duration, fav.date, fav.source_city, "
				+ "fav.source_state, fav.source_country, fav.city, fav.state, "
				+ "fav.country, fr.customer_id "
				+ "FROM Flight_Airport_View AS fav, Flight_Reservation AS fr "
				+ "WHERE fav.id = fr.flight_id";
				
		st.execute(create_view);
	}
	
	/**
	 * Creates car_reservation_view
	 * @param st
	 * @throws SQLException
	 */
	public static void car_reservation_view(Statement st) throws SQLException {
		st.execute("DROP VIEW IF EXISTS Car_Reservation_View CASCADE");
		
		String create_view = "CREATE VIEW Car_Reservation_View AS "
				+ "SELECT cr.id, cr.car_id, cr.customer_id, cr.date, cr.duration, cr.cost, "
				+ "crc.name, crc.street_address, crc.zip_code, crc.city, crc.state, crc.country, crc.phone_number "
				+ "FROM Car_Rental_Company AS crc, Car_Reservation AS cr "
				+ "WHERE cr.car_rental_company_id = crc.id";
				
		st.execute(create_view);
	}
	
	/**
	 * Creates hotel_reservation_view
	 * @param st
	 * @throws SQLException
	 */
	public static void hotel_reservation_view(Statement st) throws SQLException {
		st.execute("DROP VIEW IF EXISTS Hotel_Reservation_View CASCADE");
		
		String create_view = "CREATE VIEW Hotel_Reservation_View AS "
				+ "SELECT h.name, h.street_address, h.zip_code, h.city, h.state, h.country, h.phone_number, "
				+ "h.stars, hr.room_id, hr.customer_id, hr.date, hr.duration, hr.cost "
				+ "FROM Hotel_Reservation AS hr, Hotel AS h "
				+ "WHERE hr.hotel_id = h.id";
					
		st.execute(create_view);
	}
	
	/**
	 * Creates tour_reservation_view
	 * @param st
	 * @throws SQLException
	 */
	public static void tour_reservation_view(Statement st) throws SQLException {
		st.execute("DROP VIEW IF EXISTS Tour_Reservation_View CASCADE");
		
		String create_view = "CREATE VIEW Tour_Reservation_View AS "
				+ "SELECT tr.customer_id, tr.cost, tr.date, tc.name, tc.street_address, tc.zip_code, tc.city, "
				+ "tc.state, tc.country, tc.phone_number "
				+ "FROM Tour_Reservation AS tr, Tour_Company AS tc "
				+ "WHERE tr.tour_company_id = tc.id";
					
		st.execute(create_view);
	}

}
