import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchMultipleTables {
    ArrayList<String> tables = new ArrayList<String>();
    ArrayList<Double> probabilities = new ArrayList<Double>();
    String expression;

    SearchMultipleTables(ArrayList<String> arraystr, String str){
        this.tables = arraystr;
        this.expression = str;
        for(String s : tables){
            probabilities.add(0.0);
        }
    }
    void calculateProbabilities(){
        for(int i = 0; i < tables.size(); i++){
            int temp = i;
            while(i+1 < tables.size()){
                if(tables.get(temp).equals(tables.get(i+1))) {
                    probabilities.set(temp, probabilities.get(temp) + 1);
                    tables.remove(i+1);
                    probabilities.remove(i+1);
                }
                i++;
            }
            i=temp;
        }
        for(Object d : probabilities){
            System.out.println(d);
        }
    }
    ArrayList<String> createQueries(){


        String str = getMaxValue(probabilities);
        ArrayList<String> returnValue = new ArrayList<String>();
        ArrayList<String> inputTables = new ArrayList<String>();
        inputTables.add(str);
        returnValue.add(createQuery(inputTables,expression));
        for(String s : tables){
            inputTables.add(s);
            returnValue.add(createQuery(inputTables,expression));
            inputTables.remove(1);
        }

        return returnValue;
    }
    public String getMaxValue(ArrayList<Double> list) {
        Double max = -999999.0;
        int pos = 0;
        for(int i = 0; i < probabilities.size(); i++){
            if(probabilities.get(i) > max) {
                max = probabilities.get(i);
                pos = i;
            }

        }
        probabilities.remove(pos);
        String returnValue = tables.get(pos);
        tables.remove(pos);
        return returnValue;

    }


    public String createQuery(ArrayList<String> tables, String expression){
        String query = "";
        if(tables.size() == 1){
            query = "SELECT * FROM t0 WHERE t0.exp";
            String t0 = "t0";
            Pattern p = Pattern.compile(t0);
            Matcher m = p.matcher(query);
            query = m.replaceAll(tables.get(0));
            String exp = "exp";
            p = Pattern.compile(exp);
            m = p.matcher(query);
            query = m.replaceAll(expression.substring(2));
        }
        if(tables.size() == 2){
            query = "SELECT * FROM t0, t1 WHERE t0.customer_id = t1.customer_id AND t0.exp";

            String t0 = "t0";
            String t1 = "t1";
            Pattern p = Pattern.compile(t0);
            Matcher m = p.matcher(query);
            query = m.replaceAll(tables.get(0));
            p = Pattern.compile(t1);
            m = p.matcher(query);
            query = m.replaceAll(tables.get(1));
            String exp = "exp";
            p = Pattern.compile(exp);
            m = p.matcher(query);
            query = m.replaceAll(expression.substring(2));
        }

        return query;
    }

}
