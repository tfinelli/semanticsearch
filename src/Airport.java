import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Reads in grouped/related airport data from airports.tsv
 * Then converts each row of airport data into a Airport object which is stored in 
 * the ArrayList airports.
 * @author Tom
 *
 */
public class Airport {
	static ArrayList<Airport> airports = new ArrayList<Airport>();
	
	String name;
	String code;
	String city;
	String zip_code;
	String state;
	String country;
	
	/**
	 * Constructor for an airport
	 * @param name
	 * @param code
	 * @param city
	 * @param zip_code
	 * @param state
	 * @param country
	 */
	public Airport(String name, String code, String city, String zip_code, String state, String country) {
		this.name = name;
		this.code = code;
		this.city = city;
		this.zip_code = zip_code;
		this.state = state;
		this.country = country;
		
		airports.add(this);		
	}
	
	/**
	 * Get a random airport from the list of airports
	 * @return
	 */
	public static Airport get_random() {
		return airports.get(DataGenerator.generate_number(0, airports.size() - 1));
	}
	
	/**
	 * Read in the airports.tsv file and convert each row into a Airport object
	 * which is added to the list of airports.
	 */
	public static void read_file() {
		BufferedReader file = null;
		
		try {
			file = new BufferedReader(new FileReader("src/airports.tsv"));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		
		try {
			String line = file.readLine();
			 
			while (line != null) {				
				String[] result = line.split("\t");
				
				new Airport(result[0], result[1], result[2], result[3], result[4], result[5]);
				
				line = file.readLine();
			}
			
			file.close();
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
