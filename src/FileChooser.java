
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class is to loading an ontology file. The file format must be json.
 */
class FileChooser extends JFrame implements ActionListener{

    JLabel label_fn;    //label
    JLabel label_msg;
    File file;
    JsonNode root;
    JButton button = new JButton("Choose file");
    DatabaseConnector databaseConnector;
    Ontology ontology;


    FileChooser(DatabaseConnector databaseConnector){
        this.databaseConnector = databaseConnector;
        button.addActionListener(this);

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(button);
        button.setActionCommand("OPEN");

        label_fn = new JLabel();
        label_fn.setText("Please open the ontology file");
        label_msg = new JLabel();
        label_msg.setVisible(false);


        JPanel labelPanel = new JPanel();
        labelPanel.add(label_fn);
        labelPanel.add(label_msg);

        getContentPane().add(labelPanel, BorderLayout.CENTER);
        getContentPane().add(buttonPanel, BorderLayout.PAGE_END);

    }

    public void actionPerformed(ActionEvent e){
        JFileChooser filechooser = new JFileChooser();
        filechooser.setCurrentDirectory(new File("src"));
        if(e.getActionCommand().equals("OPEN")) {
            int selected = filechooser.showOpenDialog(this);
            if (selected == JFileChooser.APPROVE_OPTION) {
                file = filechooser.getSelectedFile();
                label_fn.setText(file.getName() + " is loaded");
                ObjectMapper mapper = new ObjectMapper();
                try {
                    //create objectmapper of ontology map
                    root = mapper.readTree(file);
                    //root -> array[0] which is trip ontology -> "name" which is ["trip", "vacation"] -> name[0] which is "trip"
                    //System.out.println(root.get(0).get("name").get(0).asText());
                    //root -> array[1] which is location ontology -> "words" which is [] -> [1] which has name:["domestic", "us"] and words[]
                    // -> list of regions -> [0] of list of regions which is "new england" -> words[] in new england -> words[0] is massachusetts
                    //System.out.println(root.get(1).get("words").get(1).get("words").get(0).get("words").get(0).get("name").get(0).asText());
                    label_msg.setText("Success. An ontology map is created.");
                    label_msg.setForeground(Color.GREEN);
                    label_msg.setVisible(true);
                    button.setText("OK");
                    button.setActionCommand("OK");
                    ArrayList<String> strs = new ArrayList<String>();
                    strs.add("ontology");
                    ontology = new Ontology(strs);
                    for(JsonNode node : root){
                        jsonToTree(node, ontology);
                    }
                    ontology.printOntology();


                } catch (IOException e_file) {
                    label_msg.setText("Failed to create ontology map. Please try again.");
                    label_msg.setForeground(Color.RED);
                    label_msg.setVisible(true);
                    System.out.println("error on json file format");
                    e_file.printStackTrace();

                }

            } else if (selected == JFileChooser.CANCEL_OPTION) {
                label_fn.setText("Cancelled");
            } else if (selected == JFileChooser.ERROR_OPTION) {
                label_fn.setText("Error");
            }
        }
        else if(e.getActionCommand().equals("OK")){
            this.setVisible(false);
            SemanticSearch semanticSearch = new SemanticSearch(this);
            semanticSearch.setDefaultCloseOperation(EXIT_ON_CLOSE);
            semanticSearch.setSize(600, 170);
            semanticSearch.setLocationRelativeTo(null);
            semanticSearch.setTitle("Semantic Search");
            semanticSearch.setVisible(false);
            semanticSearch.setVisible(true);

        }
    }
    public File getFile(){
        return file;
    }

    public boolean jsonToTree(JsonNode node, Ontology ontology){
        //System.out.println("node.size:" + node.size());
        ArrayList<String> strs = new ArrayList<String>();
        for(JsonNode s : node.get("name")){
            strs.add(s.asText());
            //System.out.print(s.asText());
        }
        Ontology temp = new Ontology(strs);
        //System.out.println();
        //System.out.println(node.get("children").get(0).get("name"));
        if(node.get("children").size() != 0){
            for(JsonNode l : node.get("children")){
                if(l.size() != 0) {
                    jsonToTree(l, temp);
                }
            }
        }
        ontology.children.add(temp);





        return true;
    }


}