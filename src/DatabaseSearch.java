import org.postgresql.core.v3.QueryExecutorImpl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to create queries.
 */
public class DatabaseSearch {
    ArrayList<MappingElement> elementList;
    ArrayList<TableList> tables;
    ArrayList<ExpressionWithP> expressions = new ArrayList<ExpressionWithP>();
    ArrayList<QueryWithP> queries = new ArrayList<QueryWithP>();
    ArrayList<String> identifierList = new ArrayList<String>();
    int baseTableNum;
    int expresionTypes = 0;

    /**
     * Constructor
     * @param elementList_  Arraylist of mapping elements which have all information about expressions from the mapping table.
     * @param tables_   Arraylist of tables related to the user input.
     */
    DatabaseSearch(ArrayList<MappingElement> elementList_, ArrayList<TableList> tables_){
        this.elementList = elementList_;
        this.tables = tables_;
        this.baseTableNum = 0;
        loadIdentifier("src/identifier.tsv");

        for(TableList tl : tables){
            System.out.println(tl.tableName + tl.count);
        }
        System.out.println();
        countTables();
        for(TableList tl : tables){
            System.out.println(tl.tableName + tl.count);
        }
        System.out.println();
        sortTables();
        for(TableList tl : tables){
            System.out.println(tl.tableName + tl.count);
        }

        baseTableNum = findNumOfMax();
        getBaseTableNumber();

        setProbability();
        for(TableList tl : tables){
            System.out.println(tl.tableName + tl.count + " "+ tl.probability);
        }
        expresionTypes = cleanExpressions();

        for(MappingElement el: elementList){
            System.out.println(el.name + " "+el.probabilities);
        }
        matchExpressions();
        System.out.println("base: " + baseTableNum);
        //createQueries();
        createQuery();
        sortQueries();
        for(QueryWithP qp: queries)System.out.println(qp.query + " " + qp.probability);
    }

    /**
     * This method will find all possible combinations of expressions in the elementList.
     * All combinations will be stored in the araylist called expressions.
     */
    public void matchExpressions(){
        ArrayList<Integer> expressionNum = new ArrayList<Integer>();
        int total = 1;
        for(int i = 0; i < expresionTypes; i++){
            expressionNum.add(1);
        }

        for(int i = 0; i < expresionTypes; i++){
            for(int j = 0; j < elementList.size()-1; j++){
                if(elementList.get(j).name.equals(elementList.get(j+1).name)){
                    expressionNum.set(i,expressionNum.get(i)+1);
                }
                else{
                    i++;

                }
            }

        }
        for(Integer i : expressionNum){
            total*=i;
        }
        System.out.println("total"+total);
        int endpoint = 0;
        int subtotal = total/expressionNum.get(0);
            for(int j = 0; j <expressionNum.get(0); j++){
                for(int k = 0 ; k <subtotal && j<elementList.size(); k++){
                    expressions.add(new ExpressionWithP(elementList.get(j).expression, (double)elementList.get(j).probabilities));
                }
            }
        endpoint += expressionNum.get(0);
        for(int a = 1; a < expresionTypes;a++){
            subtotal /= expressionNum.get(a);
            for(int i = 0 ; i < total ; i++){
                for(int j = 0; j <expressionNum.get(a); j++){
                    for(int k = endpoint ; k <endpoint+expressionNum.get(a); k++){
                        expressions.get(i).expression += " AND " + elementList.get(k).expression;
                        expressions.get(i).probability *= elementList.get(k).probabilities;
                    }
                }

            }
            endpoint += expressionNum.get(a);
        }
        for(ExpressionWithP ep: expressions){
            System.out.println(ep.expression + ep.probability);
        }
        return;
    }

    /**
     * This method will be called when the class is initiated.
     * It will load the table of JOIN, and save it as an arraylist called identifierList
     * @param str
     */
    public void loadIdentifier(String str){
        try {
            FileReader fr = new FileReader(str);
            BufferedReader br = new BufferedReader(fr);

            String line;
            StringTokenizer token;
            while ((line = br.readLine()) != null) {
                token = new StringTokenizer(line, "\t");
                while (token.hasMoreTokens()) {
                    identifierList.add(token.nextToken());
                }

            }
            br.close();
            for(String tok : identifierList){
                System.out.println(tok);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method will creates all possible queries with probabilities and save them in an arraylist called queries.
     * Any number of tables and expressions are supported.
     * @return
     */
    public String createQueries(){
            String query = "";
            double prb = 0;
            query = "SELECT * FROM t0";                         //SELECT * FROM t0
            for(int i = 1; i < baseTableNum; i++){
                query += ", t" + i;
            }
                                                                //SELECT * FROM t0, t1
        query += " EXTRA_TABLE";
                                                                //SELECT * FROM t0, t1 EXTRA_TABLE
            if(true) {
                if (baseTableNum > 1) {
                    query += " WHERE t0.t_id = t1.t_id";
                    for (int i = 2; i < baseTableNum; i++) {
                        query += " AND t0.t_id = t" + i + ".t_id";
                    }

                }
                //base table > 1
                //SELECT * FROM t0, t1 EXTRA_TABLE WHERE t0.t_id = t1.t_id
                //else
                //SELECT * FROM t0, t1 EXTRA_TABLE

                //base table > 1
                //SELECT * FROM t0, t1 EXTRA_TABLE WHERE t0.t_id = t1.t_id
                //else
                //SELECT * FROM t0, t1 EXTRA_TABLE

                ArrayList<String> tableNames = new ArrayList<String>();
                for (int i = 0; i < baseTableNum; i++) {
                    tableNames.add(tables.get(i).tableName);
                }

                String t = "t_id";
                Pattern p = Pattern.compile(t);
                Matcher m = p.matcher(query);
                query = m.replaceAll(getIdentifier(tableNames));
                //SELECT * FROM t0, t1 EXTRA_TABLE WHERE t0.t_cutomerid = t1.customerid


                String temp = query;
                String extraT = " EXTRA_TABLE";
                p = Pattern.compile(extraT);
                m = p.matcher(temp);
                temp = m.replaceAll("");
                //SELECT * FROM t0, t1  WHERE t0.t_cutomerid = t1.customerid

                String baseOnly = temp;
                if (expressions.size() > 0) {

                    for (ExpressionWithP ep : expressions) {
                        temp = baseOnly;
                        if (baseTableNum == 1) temp += " WHERE";
                        else temp += " AND";
                        if(ep.expression.startsWith("*.")) {
                            temp += " t0." + ep.expression.substring(2);
                        }else{
                            temp += " " + ep.expression;
                        }
                        for (int i = 1; i < baseTableNum; i++) {
                            if(ep.expression.startsWith("*.")) {
                                temp += " AND t" + i + "." + ep.expression.substring(2);
                            }else{
                                temp += " AND " + ep.expression;
                            }
                        }
                        for (int i = 0; i < baseTableNum; i++) {
                            t = "t" + i;
                            p = Pattern.compile(t);
                            m = p.matcher(temp);
                            temp = m.replaceAll(tables.get(i).tableName);
                            prb += tables.get(i).probability;
                        }
                        queries.add(new QueryWithP(temp, prb * ep.probability));
                        prb = 0;
                    }
                } else {
                    for (int i = 0; i < baseTableNum; i++) {
                        t = "t" + i;
                        p = Pattern.compile(t);
                        m = p.matcher(temp);

                        temp = m.replaceAll(tables.get(i).tableName);
                        prb += tables.get(i).probability;
                    }
                    queries.add(new QueryWithP(temp, prb * 100));
                    prb = 0;
                }

                for (int i = 0; i < baseTableNum; i++) {
                    t = "t" + i;
                    p = Pattern.compile(t);
                    m = p.matcher(query);
                    query = m.replaceAll(tables.get(i).tableName);

                }

                for (int i = baseTableNum; i < tables.size(); i++) {
                    temp = query;
                    p = Pattern.compile(extraT);
                    m = p.matcher(temp);
                    temp = m.replaceAll(" ," + tables.get(i).tableName);
                    if (baseTableNum == 1) temp += " WHERE t0.t_id = " + tables.get(i).tableName + ".t_id";
                    else temp += " AND t0.t_id = " + tables.get(i).tableName + ".t_id";

                    String baseAndExtra = temp;
                    if (expressions.size() > 0) {
                        for (ExpressionWithP ep : expressions) {
                            temp = baseAndExtra;
                            temp += " AND";
                            if(ep.expression.startsWith("*.")) {
                                temp += " t0." + ep.expression.substring(2);
                            }else{
                                temp += " " + ep.expression;
                            }
                            for (int j = 1; j < baseTableNum; j++) {
                                if(ep.expression.startsWith("*.")) {
                                    temp += " AND t" + j + "." + ep.expression.substring(2);
                                }else{
                                    temp += " AND " + ep.expression;
                                }
                            }
                            if(ep.expression.startsWith("*.")) {
                                temp += " AND t" + tables.get(i).tableName + "." + ep.expression.substring(2);
                            }else{
                                temp += " AND " + ep.expression;
                            }
                            for (int j = 0; j < baseTableNum; j++) {
                                t = "t" + j;
                                p = Pattern.compile(t);
                                m = p.matcher(temp);
                                temp = m.replaceAll(tables.get(j).tableName);
                                prb += tables.get(j).probability;
                            }
                            prb *= tables.get(i).probability;
                            while(tableNames.size()>0){
                                tableNames.remove(0);
                            }
                            for (int j = 0; j < baseTableNum; j++) {
                                tableNames.add(tables.get(j).tableName);
                            }
                            tableNames.add(tables.get(i).tableName);
                            for(String tl : tableNames){
                                System.out.println(tl);
                            }
                            t = "t_id";
                            p = Pattern.compile(t);
                            m = p.matcher(temp);
                            temp = m.replaceAll(getIdentifier(tableNames));
                            queries.add(new QueryWithP(temp, prb * ep.probability));
                            prb = 0;
                        }
                    } else {
                        temp = query;
                        p = Pattern.compile(extraT);
                        m = p.matcher(temp);
                        temp = m.replaceAll(" ," + tables.get(i).tableName);
                        if (baseTableNum == 1) temp += " WHERE t0.t_id = " + tables.get(i).tableName + ".t_id";
                        else temp += " AND t0.t_id = " + tables.get(i).tableName + ".t_id";
                        for (int j = 0; j < baseTableNum; j++) {
                            t = "t" + j;
                            p = Pattern.compile(t);
                            m = p.matcher(temp);
                            temp = m.replaceAll(tables.get(j).tableName);
                            prb += tables.get(j).probability;
                        }
                        while(tableNames.size()>0){
                            tableNames.remove(0);
                        }
                        for (int j = 0; j < baseTableNum; j++) {
                            tableNames.add(tables.get(j).tableName);
                        }
                        tableNames.add(tables.get(i).tableName);
                        t = "t_id";
                        p = Pattern.compile(t);
                        m = p.matcher(temp);
                        temp = m.replaceAll(getIdentifier(tableNames));
                        prb *= tables.get(i).probability;
                        queries.add(new QueryWithP(temp, prb * 100));
                        prb = 0;

                    }
                }
            }


        return query;
    }
    /**
     * This method will creates all possible queries with probabilities and save them in an arraylist called queries.
     * The number of tables appears most must be smaller than 3.
     * @return
     */
    public String createQuery(){
        String query = "SELECT * FROM t0";
        double prb = 0;                      //SELECT * FROM t0
        for(int i = 1; i < baseTableNum; i++){
            query += ", t" + i;
        }
        query += " EXTRA_TABLE";
        if (baseTableNum > 1) {
            query += " WHERE t0.t_id = t1.t_id";
            for (int i = 2; i < baseTableNum; i++) {
                query += " AND t0.t_id = t" + i + ".t_id";
            }
        }


        ArrayList<String> tableNames = new ArrayList<String>();

        String t;
        Pattern p;
        Matcher m;
        //SELECT * FROM t0, t1 EXTRA_TABLE WHERE t0.cutomer_id = t1.customer_id

        String temp = query;
        String extraT = " EXTRA_TABLE";
        p = Pattern.compile(extraT);
        m = p.matcher(temp);
        temp = m.replaceAll("");
        String e = "";
        //SELECT * FROM t0, t1 WHERE t0.t_cutomerid = t1.customerid
        String baseQuery = temp;
        if(expressions.size() > 0){
            for(int i = 0; i < expressions.size() ; i++){
                temp = baseQuery;
                e = expressions.get(i).expression;
                t = "\\*";
                p = Pattern.compile(t);
                m = p.matcher(e);
                e = m.replaceAll(tables.get(0).tableName);


                if(baseTableNum == 1){
                    temp += " WHERE " + e;
                }else{
                    temp += " AND " + e;
                }
                for(int j = 1; j < baseTableNum; j++){
                    e = expressions.get(i).expression;
                    t = "\\*";
                    p = Pattern.compile(t);
                    m = p.matcher(e);
                    e = m.replaceAll(tables.get(j).tableName);
                    temp += " AND " + e;
                }
                for (int j = 0; j < baseTableNum; j++) {
                    t = "t" + j;
                    p = Pattern.compile(t);
                    m = p.matcher(temp);

                    temp = m.replaceAll(tables.get(j).tableName);
                    prb += tables.get(j).probability;
                }
                for (int j = 0; j < baseTableNum; j++) {
                    tableNames.add(tables.get(j).tableName);
                }
                t = "t_id";
                p = Pattern.compile(t);
                m = p.matcher(temp);
                temp = m.replaceAll(getIdentifier(tableNames));
                queries.add(new QueryWithP(temp, prb * expressions.get(i).probability));
                prb = 0;
            }
        }else {
            for (int i = 0; i < baseTableNum; i++) {
                t = "t" + i;
                p = Pattern.compile(t);
                m = p.matcher(temp);

                temp = m.replaceAll(tables.get(i).tableName);
                prb += tables.get(i).probability;
            }
            for (int j = 0; j < baseTableNum; j++) {
                tableNames.add(tables.get(j).tableName);
            }
            t = "t_id";
            p = Pattern.compile(t);
            m = p.matcher(temp);
            temp = m.replaceAll(getIdentifier(tableNames));
            queries.add(new QueryWithP(temp, prb * 100));
            prb = 0;
        }
        //for matching base and others
        for(int a = baseTableNum; a < tables.size(); a++){
            temp = query;
            p = Pattern.compile(extraT);
            m = p.matcher(temp);
            temp = m.replaceAll(", " +tables.get(a).tableName);
            if(baseTableNum == 1){
                temp += " WHERE t0.t_id = t" + a + ".t_id";
            }else{
                temp += " AND t0.t_id = t" + a + ".t_id";
            }
            baseQuery = temp;
            if(expressions.size() > 0){
                for(int i = 0; i < expressions.size() ; i++){
                    temp = baseQuery;
                    e = expressions.get(i).expression;
                    t = "\\*";
                    p = Pattern.compile(t);
                    m = p.matcher(e);
                    e = m.replaceAll(tables.get(0).tableName);
                    temp += " AND " + e;

                    for(int j = 1; j < baseTableNum; j++){
                        e = expressions.get(i).expression;
                        t = "\\*";
                        p = Pattern.compile(t);
                        m = p.matcher(e);
                        e = m.replaceAll(tables.get(j).tableName);
                        temp += " AND " + e;
                    }
                    for (int j = 0; j < baseTableNum; j++) {
                        t = "t" + j;
                        p = Pattern.compile(t);
                        m = p.matcher(temp);

                        temp = m.replaceAll(tables.get(j).tableName);
                        prb += tables.get(j).probability;
                    }
                    e = expressions.get(i).expression;
                    t = "\\*";
                    p = Pattern.compile(t);
                    m = p.matcher(e);
                    e = m.replaceAll(tables.get(a).tableName);
                    temp += " AND " + e;
                    t = "t" + a;
                    p = Pattern.compile(t);
                    m = p.matcher(temp);
                    temp = m.replaceAll(tables.get(a).tableName);
                    prb *= tables.get(a).probability;
                    while(tableNames.size() > 0){
                        tableNames.remove(0);
                    }
                    for (int j = 0; j < baseTableNum; j++) {
                        tableNames.add(tables.get(j).tableName);
                    }
                    tableNames.add(tables.get(a).tableName);
                    t = "t_id";
                    p = Pattern.compile(t);
                    m = p.matcher(temp);
                    temp = m.replaceAll(getIdentifier(tableNames));
                    queries.add(new QueryWithP(temp, prb * expressions.get(i).probability));
                    prb = 0;
                }
            }else {
                for (int i = 0; i < baseTableNum; i++) {
                    t = "t" + i;
                    p = Pattern.compile(t);
                    m = p.matcher(temp);

                    temp = m.replaceAll(tables.get(i).tableName);
                    prb += tables.get(i).probability;
                }
                t = "t" + a;
                p = Pattern.compile(t);
                m = p.matcher(temp);
                temp = m.replaceAll(tables.get(a).tableName);
                prb *= tables.get(a).probability;
                while(tableNames.size() > 0){
                    tableNames.remove(0);
                }
                for (int j = 0; j < baseTableNum; j++) {
                    tableNames.add(tables.get(j).tableName);
                }
                tableNames.add(tables.get(a).tableName);
                t = "t_id";
                p = Pattern.compile(t);
                m = p.matcher(temp);
                temp = m.replaceAll(getIdentifier(tableNames));
                queries.add(new QueryWithP(temp, prb * 100));
                prb = 0;
            }


        }


        return query;
    }

    /**
     * This method will find an identifier among multiple tables for JOIN from the table loaded by loadIdentifier(String).
     *
     * @param tables It takes a list of tables which will be JOIN.
     * @return  {@code null} if identifier cannot be found, {@code String} returns found identifier
     */
    public String getIdentifier(ArrayList<String> tables){
        System.out.println(identifierList.size());


        int j = 0;
        int c = 1;
        boolean flag = false;
        for(int i = 0 ; i < identifierList.size();){
            System.out.println("comparing " + identifierList.get(i) + " and "+ tables.get(j));
            if(identifierList.get(i).equals(tables.get(j))){
                flag = true;
                while(j + c < tables.size() && i+c < identifierList.size() && flag == true){
                    System.out.println("comparing " + identifierList.get(i+c) + " and "+ tables.get(j+c)+ c);
                    if(!identifierList.get(i+c).equals(tables.get(j+c))) flag = false;
                    c++;
                }
                if(flag == true) return identifierList.get(i+3);
                else i+=4;
                c = 1;

            }
            else{
                i += 4;
            }
        }
        return null;
    }

    /**
     * This is a helper method to clean the list of expressions.
     *
     * @return {@code int} It will return number of types of expressions. For example, a word new york has 2 expressions which are
     * city and state, but they are counted as 1.
     */
    public int cleanExpressions(){
        int n=1;

        for(int i = 0; i<elementList.size();i++){
            elementList.get(i).probabilities/=100;
            if(elementList.get(i).expression.equals("null")) {
                elementList.remove(i);
                i--;
            }else{
                if(elementList.get(i).expression.startsWith("*")){
                    elementList.get(i).expression = elementList.get(i).expression;
                }

            }
        }
        for(MappingElement me: elementList){
            System.out.println(me.name);
        }
        for(int i = 0; i<1;i++){

            for(int j = i+1; j < elementList.size(); j++){
                //System.out.println(elementList.get(i).name +" "+ elementList.get(j).name);
                if(elementList.get(i).name.equals(elementList.get(j).name)){

                }else{
                    i=j;
                    n++;
                }
            }
        }
        System.out.println("n: "+ n);
        return n;
    }

    /**
     * This method will set the probabilities of tables.
     */
    public void setProbability(){
        int total = tables.size();
        int baseCount = 0;
        for(int i = 0 ; i< baseTableNum; i++){
            baseCount += tables.get(i).count;
        }
        for(int i = 0; i < tables.size(); i ++){
            if(i < baseTableNum){

                tables.get(i).probability = ((double)baseCount/total);
            }
            else{
                tables.get(i).probability = ((double)tables.get(i).count/total);
            }
        }

    }

    /**
     * This method will find the number of tables which appear most. It will put the value in baseTableNum.
     */
    public void getBaseTableNumber(){
        int temp = 0;
        for(TableList tl : tables){
            if(tables.get(0).count == tl.count) temp++;
        }
        baseTableNum = temp;
    }

    /**
     * This does same thing as getBaseTableNumber() does but return the value instead. This should not be used.
     * @return The number of tables which appear most
     */
    public int findNumOfMax(){
        int n = 0;
        int i = 0;
        //System.out.println("n: " + n);
        while(i < tables.size() && tables.get(0).count == tables.get(i).count){
            //System.out.println("0 i: " + tables.get(0).count + " " + tables.get(i).count);
            n++;
            i++;
        }
        return n;
    }

    /**
     * This method will sort the table in descending order from the table appears most.
     */
    public void sortTables(){
        for(int i = 0; i < tables.size()-1; i++){
            if(tables.get(i).count < tables.get(i+1).count){
                TableList temp = tables.get(i);
                tables.set(i, tables.get(i+1));
                tables.set(i+1, temp);
                i = 0;
            }
        }
    }

    /**
     * This method will sort the queries in descending order from high probability.
     */
    public void sortQueries(){
        for(int i = 0; i < queries.size()-1; i++){
            if(queries.get(i).probability < queries.get(i+1).probability){
                QueryWithP temp = queries.get(i);
                queries.set(i, queries.get(i+1));
                queries.set(i+1, temp);
                i = 0;
            }
        }
    }

    /**
     * This method will count how many times each tables appeared.
     */
    public void countTables(){
        //System.out.println("count started");
        for(int i = 0; i < tables.size(); i++){
            for(int j = i+1; j < tables.size(); j++){
                //System.out.println("i=" +i + ", tablename:" + tables.get(i).tableName + " and " + tables.get(j).tableName);
                if(tables.get(i).tableName.equals(tables.get(j).tableName)){
                    //System.out.println("equal");
                    tables.get(i).count += tables.get(j).count;
                    tables.remove(j);
                    i =0;
                    j = i+1;
                }
            }

        }
    }

}
