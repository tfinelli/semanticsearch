import java.sql.SQLException;
import java.sql.Statement;
/**
 * Copies data from the each of the TSV files into the database.
 * Each TSV represents one table in the database (e.g. hotel.tsv --> Hotel table in database).
 * This is how each database table is populated with data.
 * 
 * @author Tom
 *
 */
public class DatabasePopulator {

	public static void populate(Statement st) throws SQLException {
		
		String[] tables = {"customer", "tour_company", "tour_reservation", "room", "hotel", "hotel_reservation", "airport", 
				"airline", "flight", "flight_reservation", "car", "car_rental_company", "car_reservation"};
		
		
		String[] table_columns = {
				"first_name, last_name, street_address, zip_code, state, country, phone_number", // customer
				"name, street_address, city, zip_code, state, country, phone_number", // tour_company
				"customer_id, tour_company_id, cost, date", // tour_reservation
				"number, type", // room
				"name, street_address, city, zip_code, state, country, phone_number, stars", // hotel
				"room_id, customer_id, hotel_id, date, duration, cost", // hotel_reservation
				"name, code, street_address, city, zip_code, state, country, phone_number", // airport
				"name, street_address, city, state, zip_code, country, phone_number", // airline
				"source_airport_id, destination_airport_id, airline_id, cost, duration, date", // flight
				"flight_id, customer_id", // flight_reservation
				"make, model, year, color, mpg", // car
				"name, street_address, city, state, zip_code, country, phone_number", // car_rental_company
				"car_id, customer_id, car_rental_company_id, date, duration, cost" // car_reservation
		};
		
		// Copy the data from the .tsv files into the database
		for (int i = 0; i < tables.length; i++) {
			st.execute("COPY " + tables[i] + "(" + table_columns[i] + ") FROM '" + System.getProperty("user.dir") + "/src/data/" + tables[i] + ".tsv' DELIMITER E'\t' NULL AS 'null';");
		}
		
	}
}
