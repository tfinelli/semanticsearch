import static javax.swing.JFrame.EXIT_ON_CLOSE;

import java.io.IOException;
import java.sql.*;

/**
 * The main class which is used to start the tool.
 * @author Tom & Yo
 *
 */
public class Main {
	
	public static void main(String[] args) {
		
		// Attempt to load the postgresql driver
		try {
			System.out.println("Loading postgresql driver.");
			Class.forName("org.postgresql.Driver");
			System.out.println("postgresql driver loaded.");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("ERROR: Could not load postgresql driver.");
			System.exit(1);
		}
		
		// Set the the default database connection parameters
		String url = "jdbc:postgresql://localhost/semanticsearch";
		String username = "postgres";
		String password = "root";
		

		try {
			Connection db = DriverManager.getConnection(url, username, password);
			Statement st = db.createStatement();
			
			// Create the tables for the database
			DatabaseGenerator.generate_database(st);
			
			// Generate random/semi-random data which can be inserted into each of the database tables.
			// Additionally, write this random/semi-random generated data to .tsv files, one for each table name.
			DatabaseDataGenerator.generate_database_data();
			
			// Read in the data from each of the .tsv files and insert it into each respective table.
			DatabasePopulator.populate(st);			
		}
		catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		// Begin GUI and more
		Init();
		
	}
	
    static void Init(){
        DatabaseConnector databaseConnector = new DatabaseConnector();
        databaseConnector.setDefaultCloseOperation(EXIT_ON_CLOSE);
        databaseConnector.setSize(300, 200);
        databaseConnector.setLocationRelativeTo(null);
        databaseConnector.setTitle("Connect to DB");
        databaseConnector.setVisible(true);
    }
		
}
