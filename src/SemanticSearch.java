import com.fasterxml.jackson.databind.JsonNode;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.StringTokenizer;
import java.util.Vector;
import java.io.IOException;


public class SemanticSearch extends JFrame implements ActionListener {
    ArrayList<String> mapKeywords = new ArrayList<String>();
    ArrayList<String> keywords = new ArrayList<String>();
    JsonNode root;
    JButton button;
    FileChooser fileChooser;
    DatabaseConnector databaseConnector;
    Ontology ontology;
    JTextField textField;
    JTextField threshold;
    Statement stmt = null;
    String sqlQuery = "hotel";
    SemanticSearch(FileChooser fileChooser){
        JPanel panel = new JPanel();
        root = fileChooser.root;
        this.fileChooser =fileChooser;
        this.databaseConnector =fileChooser.databaseConnector;

        ontology = fileChooser.ontology;
        loadMapper("src/mapping.tsv");
    //the connection
        try {
            stmt = databaseConnector.con.createStatement ();
            String sql = "SELECT * FROM hotel";
            ResultSet rs = stmt.executeQuery (sql);
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                System.out.println("ID：" + id);
                System.out.println("name：" + name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //test the map
        textField = new JTextField(40);
        threshold = new JTextField(2);
        threshold.setText("20");
        JLabel labelSlash = new JLabel("Threshold(%)");
        button = new JButton("Search");
        button.addActionListener(this);
        button.setActionCommand("SEARCH");
        JLabel topImage = new JLabel();
        ImageIcon icon = new ImageIcon("src/logo.png");
        JLabel labelLogo = new JLabel(icon);
        panel.add(labelLogo);
        panel.add(textField);
        panel.add(button);
        panel.add(labelSlash);
        panel.add(threshold);

        this.add(panel);





    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("SEARCH")) {

            String[] userQuery = textField.getText().toLowerCase().split(" ", 0);
            String expression = null;
            String temp = null;
            ArrayList<String> expressionList = new ArrayList<String>();
            ArrayList<MappingElement> elementList = new ArrayList<MappingElement>();
            String sql;
            ArrayList<TableList> tables = new ArrayList<TableList>();
            for (int i = 0; i<userQuery.length; i++){
                //less than
                String quantity = null;
                //if(userQuery[i].equals("less") && userQuery[i+1].equals("than")) quantity = userQuery[i+2]; // keywords.add("<" + userQuery[i+2]);
                elementList.addAll(searchOntology(userQuery[i]));
                if(elementList.size()>0 && elementList.get(elementList.size()-1).probabilities == -1 && i+1 < userQuery.length){
                    elementList.addAll(searchOntology(userQuery[i] + " " + userQuery[i+1]));
                    if(elementList.get(elementList.size()-1).probabilities != -1) i++;
                }
                /*
                if(keywords.get(keywords.size()-1) == null && i+1 < userQuery.length){
                    keywords.add(searchOntology(userQuery[i] + " " + userQuery[i+1]));
                    if(keywords.get(keywords.size()-1) != null){
                        expression = getExpression(userQuery[i] + " " + userQuery[i+1]);
                        System.out.println("expression for " + userQuery[i] + " " + userQuery[i+1] + ": " + expression);
                        i++;
                    }
                }
                */

                //expression = getExpression(userQuery[i]);
                /*
                if (expression != null && !expression.equals("null")) {
                	if (quantity != null) {
                		expression.replace("{}", quantity);
                	}
                	expressionList.add(expression);
                }

                System.out.println("expression for " + userQuery[i] + ": " + expression);
                */
            }
            for(int i = 0; i < elementList.size(); i++){
                if(elementList.get(i).probabilities == -1) {
                    elementList.remove(i);
                    i--;
                }
            }

            System.out.println("********************************************");
            for(MappingElement me : elementList){
                System.out.println(me.name);
                System.out.println(me.tables);
                System.out.println(me.expression);
                System.out.println(me.probabilities);
            }
            System.out.println("********************************************");


            tables = mergeTables(elementList);
            DatabaseSearch ds = new DatabaseSearch(elementList,tables);
            int removed = 0;
            double totalProbability = 0;
            for(QueryWithP qp : ds.queries){
                totalProbability+= qp.probability;
            }
            for(QueryWithP qp : ds.queries){
                qp.probability /= totalProbability;
            }
            for(int i = 0;i < ds.queries.size(); i++){
                if(ds.queries.get(i).probability < Double.parseDouble(threshold.getText())/100){
                    ds.queries.remove(i);
                    removed++;
                    i--;
                }
            }

            /*
            for(String s : keywords){
                if(s != null) {
                    String line;
                    StringTokenizer token;
                    token = new StringTokenizer(s, ", ");
                    while (token.hasMoreTokens()) {
                        tables.add(token.nextToken());
                    }
                }
            }
            System.out.println("Result: " + tables );



            Mapping mapping = new Mapping();
            mapping.add_tables(tables);
            //mapping.add_expression(expression);
            mapping.add_expression_list(expressionList);
            */
            //SearchMultipleTables smtb = new SearchMultipleTables(tables, expression);
            //System.out.println(smtb.createQuery(tables, expression));

            /*
            String sql = mapping.generate_sql_with_expression_list();
            //System.out.println(mapping.generate_sql());
            Result result = new Result(databaseConnector);
            result.setQuery(mapping.generate_sql(), 1);
            */
            Result resultWindow = new Result(databaseConnector);

                resultWindow.setQuery(ds.queries, removed);



            keywords.clear();
            int rowNum = 50;
            int colNum = 3;
            try {
                stmt = databaseConnector.con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

                ResultSet rs;
                
                sql = "SELECT * FROM " + sqlQuery;

                rs = stmt.executeQuery(sql);

                //get row num
                rs.last();
                rowNum = rs.getRow();
                rs.beforeFirst();

                //get colnum and col names
                ResultSetMetaData metaData = rs.getMetaData();
                colNum = metaData.getColumnCount();
                String columnName[] = new String[colNum];
                for (int i = 1; i <= colNum; i++) {
                    columnName[i - 1] = metaData.getColumnLabel(i);
                }
                int n = 0;

                String[][] rowDat = new String[rowNum][colNum];
                while (rs.next()) {
                    for(int i = 1; i < colNum ;i++){
                        rowDat[n][i-1] = rs.getString(i);
                    }
                    n++;
                    int id = rs.getInt("id");
                    //String name = rs.getString("name");
                    //System.out.println("ID：" + id);
                    //System.out.println("name：" + name);
                }

                DefaultTableModel tm = new DefaultTableModel(rowDat, columnName);
                JTable tb = new JTable(tm);
                JScrollPane sp = new JScrollPane(tb);
                sp.setPreferredSize(new Dimension(800, 600));

                JPanel panelResult = new JPanel();



                /*
                resultWindow.add(panelResult, BorderLayout.CENTER);
                panelResult.add(sp);
                */
                resultWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                resultWindow.setTitle(textField.getText());
                resultWindow.setSize(800, 600);
                resultWindow.setVisible(true);



            } catch (SQLException es) {
                resultWindow = new Result(databaseConnector);
                resultWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                resultWindow.setTitle(textField.getText());
                resultWindow.add(new JLabel("Error on sql"));
                resultWindow.setSize(800, 600);
                resultWindow.setVisible(true);

                es.printStackTrace();
            }

        }
    }
    public ArrayList<MappingElement> searchBelow(Ontology ontology){
        ArrayList<MappingElement> returnValue = new ArrayList<MappingElement>();
        ArrayList<MappingElement> temp;
        boolean nullF = true;
        for(Ontology ot: ontology.children){
            temp = getMappingElementList(ot);
            System.out.println(temp);
            if(temp != null) {
                System.out.println(temp.get(0).name);
                returnValue.addAll(temp);
                nullF = false;
            }
            else{
                returnValue.addAll(searchBelow(ot));
            }

        }
        if(returnValue.size()==0) return null;
        else {

            return returnValue;
        }
    }
    public ArrayList<MappingElement> searchOntology(String str){
        Ontology result = ontology.searchString(str);
        ArrayList<MappingElement> returnValue = null;
        System.out.println("searching "+ str);
        if(result != null){
            System.out.println(result.names + "was found in ontology");
            returnValue = getMappingElementList(result);
            if(returnValue != null) {
               System.out.println("keyword is found in mapper");
                return returnValue;
            }
            else {
                returnValue=searchBelow(result);
                if(returnValue != null) {
                    System.out.println("returninn");
                    for(MappingElement el: returnValue){
                        System.out.println(el.name);
                    }
                    System.out.println("returninn");
                    return returnValue;
                }
                else {
                    // System.out.println("keyword is not found in mapper");
                    ArrayList<MappingElement> temp = new ArrayList<MappingElement>();
                    temp.add(new MappingElement());
                    return temp;
                }
            }

        }
        else {
            ArrayList<MappingElement> temp = new ArrayList<MappingElement>();
            temp.add(new MappingElement());
            return temp;
        }
    }
    public ArrayList<MappingElement> getMappingElementList(Ontology ontology){
        ArrayList<MappingElement> elementList = new ArrayList<MappingElement>();
        boolean nullF = true;
        for(int i = 0; i < mapKeywords.size(); i++){
            if(ontology.names.get(0).equals(mapKeywords.get(i))){
                System.out.println(ontology.names.get(0) + " was found in mapper");
                System.out.println("i: " + mapKeywords.get(i));
                System.out.println("i+1: " + mapKeywords.get(i+1));
                System.out.println("i+2: " + mapKeywords.get(i+2));
                System.out.println("i+3: " + mapKeywords.get(i+4));
                elementList.add(new MappingElement(ontology.names.get(0), splitTableNames(mapKeywords.get(i+2)), mapKeywords.get(i+3), Integer.parseInt(mapKeywords.get(i+4))));
                i = i+3;
                nullF = false;
            }
        }
        if(nullF) return null;
        else{
            return elementList;
        }


    }
    public ArrayList<TableList> mergeTables(ArrayList<MappingElement> list){
        ArrayList<TableList> tables = new ArrayList<TableList>();
        for(MappingElement em : list){
            for(String s: em.tables){
                tables.add(new TableList(s,1));
            }
        }
        return tables;
    }

    public ArrayList<String> splitTableNames(String str){
        ArrayList<String> tables = new ArrayList<String>();
        String line;
        StringTokenizer token;
        token = new StringTokenizer(str, ", ");
        while (token.hasMoreTokens()) {
            tables.add(token.nextToken());
        }

        return tables;
    }

    public String getExpression(String str){
        Ontology reslt = ontology.searchString(str);
        String returnValue = null;

        if(reslt != null){
            System.out.println("found" + reslt.names);
            returnValue = searchMapperExpression(reslt);
            if(returnValue != null) {
               // System.out.println("keyword is found in mapper");
                return returnValue;
            }
            else {
              //  System.out.println("keyword is not found in mapper");
                return null;
            }

        }
        else {
            System.out.println(str + " was not found in ontology");
            return null;
        }

    }
    public String searchMapperExpression(Ontology ontology){
        do{
            for(int i = 0; i < mapKeywords.size() ; i++){
                if(mapKeywords.get(i).equals(ontology.names.get(0))){

                    // System.out.println("found!");

                    return mapKeywords.get(i+3);
                }
            }
            /*
            for(String s: mapKeywords){
               // System.out.println("comparing "+ s +" and " + ontology.names.get(0));
                if(s.equals(ontology.names.get(0))){

                   // System.out.println("found!");

                    return s;
                }

            }*/
            for(Ontology o : ontology.children){
                String temp = searchMapper(o);
                if(temp != null) return temp;
            }

        }while((ontology.children.size()> 0));

        return null;
    }
    public String searchMapper(Ontology ontology){
        do{
            for(int i = 0; i < mapKeywords.size() ; i++){
                if(mapKeywords.get(i).equals(ontology.names.get(0))){

                    // System.out.println("found!");

                    return mapKeywords.get(i+3);    //number of columns
                }
            }
            /*
            for(String s: mapKeywords){
               // System.out.println("comparing "+ s +" and " + ontology.names.get(0));
                if(s.equals(ontology.names.get(0))){

                   // System.out.println("found!");

                    return s;
                }

            }*/
            for(Ontology o : ontology.children){
                String temp = searchMapper(o);
                if(temp != null) return temp;
            }

        }while((ontology.children.size()> 0));

        return null;
    }
    public void loadMapper(String str){
        try {
            FileReader fr = new FileReader(str);
            BufferedReader br = new BufferedReader(fr);

            String line;
            StringTokenizer token;
            while ((line = br.readLine()) != null) {
                token = new StringTokenizer(line, "\t");
                while (token.hasMoreTokens()) {
                    mapKeywords.add(token.nextToken());
                }

            }
            br.close();
            for(String tok : mapKeywords){
                System.out.println(tok);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
