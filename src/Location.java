import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Reads in grouped/related location data from locations.tsv
 * Then converts each row of location data into a Location object which is stored in 
 * the ArrayList locations.
 *  
 * @author Tom
 *
 */
public class Location {
	
	static int count = 0;
	
	static ArrayList<Location> locations = new ArrayList<Location>();
	
	String city;
	String state;
	String country;
	String zip_code;
	
	/**
	 * Constructor for a location
	 * @param country
	 * @param state
	 * @param city
	 * @param zip_code
	 */
	Location(String country, String state, String city, String zip_code) {
		this.city = city;
		this.state = state;
		this.country = country;
		this.zip_code = zip_code;
		
		// Add the location to the list of locations
		locations.add(this);		
	}
	
	/**
	 * Gets a random location from the list of locations
	 * @return
	 */
	public static Location get_random() {
		return locations.get(DataGenerator.generate_number(0, locations.size() - 1));
	}
	
	/**
	 * Print a location
	 */
	public void print() {
		if (this.city != null)
			System.out.print(this.city + " ");
		
		if (this.state != null)
			System.out.print(this.state + " ");
		
		if (this.country != null)
			System.out.print(this.country + " ");
		
		if (this.zip_code != null)
			System.out.print(this.zip_code + " ");
		
		System.out.println();
	}
	
	/**
	 * Read in the locations.tsv file and convert each row into a Location object
	 * which is added to the list of locations.
	 */
	public static void read_file() {
		BufferedReader file = null;
		
		try {
			file = new BufferedReader(new FileReader("src/locations.tsv"));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		
		try {
			String line = file.readLine();
			 
			while (line != null) {				
				String[] result = line.split("\t");
				
				new Location(result[0], result[1], result[2], result[3]);
				
				line = file.readLine();
			}
			
			file.close();
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}