import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Reads in grouped/related car data from cars.tsv
 * Then converts each row of car data into a Location object which is stored in 
 * the ArrayList cars.
 *  
 * @author Tom
 *
 */
public class Car {
	static ArrayList<Car> cars = new ArrayList<Car>();
	
	String make;
	String model;
	
	/**
	 * Constructor for a car
	 * @param make
	 * @param model
	 */
	public Car(String make, String model) {
		this.make = make;
		this.model = model;
		
		cars.add(this);		
	}
	
	/**
	 * Get a random car from the list of cars
	 * @return
	 */
	public static Car get_random() {
		return cars.get(DataGenerator.generate_number(0, cars.size() - 1));
	}
	
	/**
	 * Read in the cars.tsv file and convert each row into a Car object
	 * which is added to the list of cars.
	 */
	public static void read_file() {
		BufferedReader file = null;
		
		try {
			file = new BufferedReader(new FileReader("src/cars.tsv"));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		
		try {
			String line = file.readLine();
			 
			while (line != null) {				
				String[] result = line.split("\t");
				
				new Car(result[0], result[1]);
				
				line = file.readLine();
			}
			
			file.close();
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
